package com.example.todolistapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.todolistapp.Catalog.CatalogViewModel
import com.example.todolistapp.Screens.Catalog.CatalogScreen
import com.example.todolistapp.Screens.Details.DetailsScreen
import com.example.todolistapp.ui.theme.TodoListAppTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val vm = CatalogViewModel()

        super.onCreate(savedInstanceState)
        setContent {
            TodoListAppTheme {
                Surface(color = MaterialTheme.colors.background) {
                    App(vm)
                }
            }
        }
    }
}


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun App(vm: CatalogViewModel) {

    val navController = rememberNavController()
    Scaffold(
        backgroundColor = Color.White,
        content = {
            NavHost(navController = navController, startDestination = "Home") {
                composable(
                    "Detail/{catalogId}",
                    arguments = listOf(navArgument("catalogId") { type = NavType.StringType })
                ) {
                    it.arguments?.getString("catalogId")
                        ?.let { params -> DetailsScreen(id = params, vm = vm) }
                }
                composable("Home") {
                    CatalogScreen(navController = navController, vm = vm)
                }
            }
        }
    )
}
