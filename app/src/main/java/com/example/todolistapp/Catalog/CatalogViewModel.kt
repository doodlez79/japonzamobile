package com.example.todolistapp.Catalog

import CatalogItemType
import Rolls
import android.util.Log
import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import com.example.todolistapp.api.APIService
import com.example.todolistapp.types.Catalog
import com.google.gson.Gson
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException

class CatalogViewModel : ViewModel() {
    private val _catalog = mutableStateListOf<Catalog>()
    private val _catalogListRolls = mutableStateListOf<Rolls>()
    private val _detailInfoAboutItem = mutableStateOf<Rolls?>(null)
    private val gson = Gson()
    private val client = OkHttpClient()
    var errorMessage: String by mutableStateOf("")
    var detailError: String by mutableStateOf("")

    val catalog: List<Catalog>
        get() = _catalog

    val catalogItem: List<Rolls>
        get() = _catalogListRolls

    val detailRoll: MutableState<Rolls?>
        get() = _detailInfoAboutItem

    fun getDetailInfo(id: String) {
        val apiService = APIService.getInstance("https://japonza.ru/api/rolls/${id}")
        val call = client.newCall(apiService)

        try {
            call.enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        if (!response.isSuccessful) throw IOException("Unexpected code $response")
                        val body = response.body()
                        body?.apply {
                            val responseResult: Rolls = gson.fromJson(
                                string(),
                                Rolls::class.java
                            )

                            _detailInfoAboutItem.value = responseResult
                        }
                    }
                }
            })
        } catch (e: Exception) {
            detailError = e.message.toString()
        }

    }


    fun getDetailForMenu(id: String) {
        val apiService = APIService.getInstance("https://japonza.ru/api/catalogs/${id}")
        val call = client.newCall(apiService)

        try {
            call.enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        if (!response.isSuccessful) throw IOException("Unexpected code $response")
                        val body = response.body()
                        body?.apply {
                            val responseResult: CatalogItemType = gson.fromJson(
                                string(),
                                CatalogItemType::class.java
                            )
                            _catalogListRolls.clear()
                            _catalogListRolls.addAll(responseResult.rolls)
                        }

                    }
                }
            })
        } catch (e: Exception) {
            errorMessage = e.message.toString()
        }
    }

    fun getRollsList() {

        val apiService = APIService.getInstance("https://japonza.ru/api/catalogs")
        val call = client.newCall(apiService)

        try {
            call.enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        if (!response.isSuccessful) throw IOException("Unexpected code $response")
                        val body = response.body()
                        body?.apply {
                            val responseResult:Array<Catalog> = gson.fromJson(
                                string(),
                            Array<Catalog>:: class.java)
                            _catalog.addAll(responseResult)

                        }
                    }
                }
            })
        } catch (e: Exception) {
            errorMessage = e.message.toString()
        }
    }

}