package com.example.todolistapp.types

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageType(val id: Int, val url: String) : Parcelable

@Parcelize
data class Catalog(val name: String, val id: Int, val order: Int, val image: ImageType) : Parcelable