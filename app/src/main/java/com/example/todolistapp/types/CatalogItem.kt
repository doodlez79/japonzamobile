import android.os.Parcelable
import com.example.todolistapp.types.ImageType
import kotlinx.android.parcel.Parcelize

data class Rolls(
    val title: String,
    val price: Int,
    val id: Int,
    val image: ImageType,
    val description: String
)

data class CatalogItemType(
    val name: String,
    val price: Int,
    val image: ImageType,
    val description: String,
    val rolls: List<Rolls>
)