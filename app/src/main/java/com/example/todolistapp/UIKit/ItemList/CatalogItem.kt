package com.example.todolistapp.UIKit.ItemList

import RemoteImage
import Rolls
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun CatalogItem(props: Rolls, onClick: () -> Unit) {

    Row(
        modifier = Modifier
            .padding(start = 16.dp, top = 16.dp)
            .clickable(onClick = onClick)
            .fillMaxWidth()
    ) {
        RemoteImage(
            imgUrl = props.image.url,
            contentDescription = props.title,
            modifier = Modifier
                .size(50.dp)
                .clip(CircleShape),
            contentScale = ContentScale.Crop
        )
        Column(modifier = Modifier.padding(start = 10.dp)) {
            Text(text = props.title, fontSize = 16.sp)
            Text(text = "${props.price} ₽", fontSize = 16.sp, color = Color.Gray)
        }

    }
}