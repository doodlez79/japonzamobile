package com.example.todolistapp.UIKit.TabItem

import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.example.todolistapp.types.Catalog


@Composable
fun TabItem(item: Catalog, onPress: () -> Unit, active: Boolean) {
    TextButton(onClick = onPress) {
        Text(text = item.name, color = if (active) Color.Blue else Color.Black)
    }

}