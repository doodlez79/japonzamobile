package com.example.todolistapp.UIKit.TabList

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.ScrollableTabRow
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.todolistapp.UIKit.TabItem.TabItem
import com.example.todolistapp.types.Catalog

@Composable
fun TabList(list: List<Catalog>, onChange: (item: Catalog) -> Unit) {
    val selectedTab = remember {
        mutableStateOf(0)
    }

    fun onPress(index: Int, item: Catalog) {
        selectedTab.value = index
        onChange(item)
    }
    ScrollableTabRow(
        selectedTabIndex = selectedTab.value,
        modifier = Modifier
            .fillMaxWidth(),
        backgroundColor = Color.Transparent,
        edgePadding = 10.dp,
        indicator = {}
    ) {
        list.forEachIndexed { index, elem ->
            TabItem(
                item = elem,
                onPress = { onPress(index, elem) },
                active = index == selectedTab.value
            )
        }
    }
}