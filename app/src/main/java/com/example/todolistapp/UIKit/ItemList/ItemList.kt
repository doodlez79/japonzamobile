import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.example.todolistapp.UIKit.ItemList.CatalogItem
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.delay

@Composable
fun ItemList(items: List<Rolls>, onClick: (id: String) -> Unit, refreshFunc: () -> Unit) {
    val scrollState = rememberScrollState()


    val (isRefreshing, setRefreshing) = remember { mutableStateOf(false) }
    val swipeRefreshState = rememberSwipeRefreshState(isRefreshing)

    LaunchedEffect(isRefreshing) {
        if (isRefreshing) {
            delay(1000L)
            setRefreshing(false)
            refreshFunc()
        }
    }

    SwipeRefresh(
        state = swipeRefreshState,
        onRefresh = {
            setRefreshing(true)
        },
        content = {
             Column(
                modifier = Modifier
                    .fillMaxSize().fillMaxWidth()
                    .verticalScroll(enabled = true, state = scrollState)
            ) {

                items.forEach { item ->
                    CatalogItem(props = item, onClick = { onClick(item.id.toString()) })
                }

            }

        }
    )


}