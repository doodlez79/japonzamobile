package com.example.todolistapp.UIKit.ItemList

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.google.accompanist.placeholder.PlaceholderHighlight
import com.google.accompanist.placeholder.fade
import com.google.accompanist.placeholder.placeholder

@Composable
fun SkeletonItemList() {
    Row(
        modifier = Modifier.padding(start = 16.dp, top = 16.dp, end = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(modifier = Modifier.size(50.dp).clip(CircleShape).placeholder(
            visible = true,
            color = Color.LightGray,
            highlight = PlaceholderHighlight.fade(
                highlightColor = Color.White,
            ),
        ))
        Box(modifier = Modifier.padding(start = 10.dp).height(30.dp).fillMaxWidth().placeholder(
            visible = true,
            color = Color.LightGray,
            highlight = PlaceholderHighlight.fade(
                highlightColor = Color.White,
            ),
            shape = RoundedCornerShape(4.dp),
        ))
    }
}