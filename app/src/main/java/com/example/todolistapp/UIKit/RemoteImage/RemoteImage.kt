import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import coil.ImageLoader
import coil.compose.rememberImagePainter
import coil.request.CachePolicy

@Composable
fun RemoteImage(
    imgUrl: String,
    contentDescription: String,
    modifier: Modifier,
    contentScale: ContentScale
) {
    val context = LocalContext.current
    val imageLoader = remember {
        ImageLoader.Builder(context)
            .memoryCachePolicy(CachePolicy.ENABLED)
            .build()
    }
    val painter = rememberImagePainter("https://japonza.ru/api$imgUrl", imageLoader = imageLoader)

    Image(
        painter = painter,
        contentDescription = contentDescription,
        modifier = modifier,
        contentScale = contentScale
    )

}