package com.example.todolistapp.api

import android.util.Log
import okhttp3.Request

interface APIService {
    companion object {
        fun getInstance(url: String): Request {
            return Request.Builder()
                .url(url)
                .build()
        }
    }
}