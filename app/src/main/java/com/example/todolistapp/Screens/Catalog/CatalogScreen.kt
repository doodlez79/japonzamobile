package com.example.todolistapp.Screens.Catalog

import ItemList
import androidx.compose.foundation.layout.*
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import com.example.todolistapp.Catalog.CatalogViewModel
import com.example.todolistapp.UIKit.ItemList.SkeletonItemList
import com.example.todolistapp.UIKit.SearchInput.SearchInput
import com.example.todolistapp.UIKit.TabList.TabList
import com.example.todolistapp.types.Catalog

@Composable
fun CatalogScreen(navController: NavHostController, vm: CatalogViewModel) {

    LaunchedEffect(Unit, block = {
        vm.getRollsList()
    })


    val (selectedId, setSelectedId) = remember {
        mutableStateOf(0)
    }

    LaunchedEffect(selectedId, block = {
        if (vm.catalog.isNotEmpty()) {
            vm.getDetailForMenu(selectedId.toString())
        }
    })

    val mockData = remember {
        mutableStateOf(vm.catalogItem)
    }

    LaunchedEffect(vm.catalog.size, block = {
        if (vm.catalog.isNotEmpty()) {
            setSelectedId(vm.catalog[0].id)
        }
    })

    val (searchValue, setSearchValue) = remember {
        mutableStateOf("")
    }

    LaunchedEffect(searchValue) {
        if (searchValue.isNotEmpty()) {
            mockData.value =
                mockData.value.filter { data ->
                    data.title.contains(
                        searchValue,
                        ignoreCase = true
                    )
                }
        } else {
            mockData.value = vm.catalogItem
        }

    }


    fun onChange(item: Catalog) {
        setSelectedId(item.id)
    }

    Scaffold(
        topBar = {
            SearchInput(searchValue = searchValue, setSearchValue = setSearchValue)
        }, content = {
            if (vm.errorMessage.isEmpty()) {
                if (mockData.value.isNotEmpty()) {
                    Column {
                        TabList(
                            list = vm.catalog,
                            onChange = { item -> onChange(item) })
                        ItemList(
                            items = mockData.value,
                            onClick = { id ->
                                navController.navigate(
                                    "Detail/${id}"
                                )
                            },
                            refreshFunc = { vm.getRollsList() },
                        )
                    }
                } else {
                    Column {
                        val skeletonArr = Array(8) { 0 }
                        skeletonArr.forEach {
                            SkeletonItemList()
                        }
                    }
                }
            } else {
                Text(vm.errorMessage)
            }
        })

}