package com.example.todolistapp.Screens.Details

import RemoteImage
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import com.example.todolistapp.Catalog.CatalogViewModel
import com.example.todolistapp.UIKit.SkeletonDateilScreen.SkeletonDetailScreen

@Composable
fun DetailsScreen(id: String, vm: CatalogViewModel) {

    LaunchedEffect(Unit, block = {
        vm.getDetailInfo(id)
    })

    if (vm.detailRoll.value == null) {
        SkeletonDetailScreen()
    } else {
        Column(
            modifier = Modifier
                .fillMaxWidth().padding(top = 16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier
                    .padding(bottom = 16.dp)
            ) {
                RemoteImage(
                    imgUrl = vm.detailRoll.value!!.image.url,
                    contentDescription = vm.detailRoll.value!!.title,
                    modifier = Modifier.size(100.dp).clip(CircleShape),
                    contentScale = ContentScale.Crop
                )
            }
            Column(modifier = Modifier.padding(horizontal = 16.dp), horizontalAlignment = Alignment.CenterHorizontally ) {
                Text(text = vm.detailRoll.value!!.title)
                Text(text = "${vm.detailRoll.value!!.price} ₽", color = Color.Gray)
                Text(text = "Состав:")
                Text(text = vm.detailRoll.value?.description.toString())
            }


        }

    }

}